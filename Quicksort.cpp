#include<iostream>
#include<cstdlib>
#include <windows.h> 
//#define RAND_MAX 10
using namespace std;



int dzielenie(int tab[], int przod, int tyl)
/*****************
funkcja dzielaca tablice na dwie czesci, z podzialem gzdzie w pierwszej czesi sa
mniejsze rowne x, a w drugiej wieksze
*********************/
{
	int x = tab[(przod+tyl)/2]; //obranie wartosci x
	int i = przod; 
	int j = tyl;//zmienne pomocnicze do poruszania sie po tablicy
	int tmp;// zmienna pomocnicza
	while (true)
	{
		while (tab[i] < x) 
		{
			i++; 
		}
		while (tab[j] > x)
		{
			j--;
		}

		if (i < j)
		{//zamiana miejscami, gdy j>i
			///swap(tab[i], tab[j]);
			tmp = tab[i];
			tab[i] = tab[j];
			tab[j] = tmp;
			i++; 
			j--;
		}
		else { /*break;*/return j; }
	}
	//return j;
}


void quicksort(int tab[], int przod, int tyl)
/***************
glowna czesc algorytmu, rekurencje
***************/
{
	int tmp;
	if (przod < tyl)
	{
		tmp = dzielenie(tab, przod, tyl);//podzial tablicy
		if (przod < tmp )
		{
			quicksort(tab, przod, tmp); //wykonanie algorytmu dla pierwszej czesci tablicy
		}
		if (tyl > tmp + 1)
		{
			quicksort(tab, tmp + 1, tyl);//wykonanie algorytmu dla drugiej czesci tablicy
		}
		}
}




int main()
{
	int rozmiar;
	cout << "Ilosc liczb do posortowania:" << endl;
	cin >> rozmiar;
	
	int * tab = new int[rozmiar]; //tablica dynamiczna
	//zapelnienie tablicy
	int tmp2 = rozmiar;
	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		tab[tmp] = tmp2;
		tmp2--;
		/*if (tmp < rozmiar*0.997)
		{
			tab[tmp] = tmp;
		}
		else 
		{
			tab[tmp] = rand();
		}*/
	}
	/*cout << "Wypisanie tablicy:" << endl;
	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		cout << tab[tmp] << " ";
	}*/
		LARGE_INTEGER frequency;        // ticks per second
		LARGE_INTEGER t1, t2;           // ticks
		double elapsedTime;

		// get ticks per second
		QueryPerformanceFrequency(&frequency);

		// start timer
		QueryPerformanceCounter(&t1);

		// do something

///******************************************************
	quicksort(tab, 0, rozmiar-1);
///!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	// stop timer
	QueryPerformanceCounter(&t2);
	// compute and print the elapsed time in millisec
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	cout << elapsedTime << " ms.\n";

	/*cout <<endl<< "Wypisanie posortowanej tablicy:" << endl;
	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		cout << tab[tmp] << " ";
	}
	cout <<endl;*/
	delete [] tab; //zwolnienie miejsca zajmowanego przez tablice
	system("PAUSE");
	return 0;
}