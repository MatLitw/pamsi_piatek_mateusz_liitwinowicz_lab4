#include<iostream>
#include<cstdlib>
#include <windows.h> 
using namespace std;

void scalanie(int tab[], int poczatek, int srodek, int koniec)
{
	int *tmp_tab = new int[(koniec - poczatek + 1)];
		int i = poczatek;
	int j = srodek+1;
	int k = 0;
	while (i <= srodek &&j <= koniec)
	{
		if (tab[j] < tab[i])
		{
			tmp_tab[k] = tab[j];
			j++;
		}
		else
		{
			tmp_tab[k] = tab[i];
			i++;
		}
		k++;
	}
	if(i<=srodek)
	{
		while (i <= srodek) 
		{
			tmp_tab[k] = tab[i];
			i++;
			k++;
		}
	}
	else
	{
		while (j<=koniec)
		{
			tmp_tab[k] = tab[j];
			j++;
			k++;
		}
	}
	for (i = 0; i <= (koniec - poczatek); i++)
	{
		tab[poczatek + i] = tmp_tab[i];

	}
	delete[] tmp_tab;
}

void sortowanie(int tab[], int poczatek, int koniec)
{
	int srodek;
	if (poczatek != koniec)
	{
		srodek = (poczatek + koniec) / 2;
		sortowanie(tab, poczatek, srodek);
		sortowanie(tab, srodek + 1, koniec);
		scalanie(tab, poczatek, srodek, koniec);
	}
}

int main()
{
	int rozmiar;
	cout << "Ilosc liczb do posortowania:" << endl;
	cin >> rozmiar;

	int * tab = new int[rozmiar]; //tablica dynamiczna
	int tmp2 = rozmiar;
	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		tab[tmp] = tmp2;
		tmp2--;
	}
/*	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		if (tmp < rozmiar * 0.997)
		{
			tab[tmp] = tmp;
		}
		else {
			tab[tmp] = rand();
		}
	}
	/*cout << "Wypisanie tablicy:" << endl;
	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		cout << tab[tmp] << " ";
	}*/
	LARGE_INTEGER frequency;        // ticks per second
	LARGE_INTEGER t1, t2;           // ticks
	double elapsedTime;

	// get ticks per second
	QueryPerformanceFrequency(&frequency);

	// start timer
	QueryPerformanceCounter(&t1);

	// do something
	sortowanie(tab, 0, rozmiar - 1);
	QueryPerformanceCounter(&t2);

	// compute and print the elapsed time in millisec
	elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
	cout << elapsedTime << " ms.\n";

	/*cout << endl << "Wypisanie posortowanej tablicy:" << endl;
	for (int tmp = 0; tmp < rozmiar; tmp++)
	{
		cout << tab[tmp] << " ";
	}
	cout << endl;*/
	delete[] tab; //zwolnienie miejsca zajmowanego przez tablice
	system("PAUSE");
	return 0;
}