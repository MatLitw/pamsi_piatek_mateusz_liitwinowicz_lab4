#include<iostream>
#include<cstdlib>
#include <windows.h>  
using namespace std;



void Shell(int tabela[], int rozmiar)
{

	int przerwa = rozmiar / 2; //przerwa pomiedzy sprawdzanymi elementami
	while (przerwa > 0)
	{
		for (int i = 0; i < rozmiar - przerwa; i++)
		{
			int zmienna = i + przerwa;
			int tmp = tabela[zmienna];
			
			while (zmienna >= przerwa && tabela[zmienna - przerwa] > tmp) //zmiana znaku > na < zmienia kolejnosc posortowania
			{
				tabela[zmienna] = tabela[zmienna - przerwa];
				zmienna -= przerwa;
			}
			tabela[zmienna] = tmp;
		}
		//Odstep zaprowponowany przez Shell'a
		przerwa /= 2;
	}
}
	int main()
	{
		LARGE_INTEGER frequency;        // ticks per second
		LARGE_INTEGER t1, t2;           // ticks
		double elapsedTime;
		int rozmiar;
		cout << "Ilosc liczb do posortowania:" << endl;
		cin >> rozmiar;

		int * tab = new int[rozmiar]; //tablica dynamiczna
		int tmp2 = rozmiar;
/*		for (int tmp = 0; tmp < rozmiar; tmp++)
		{
			tab[tmp] = tmp2;
			tmp2--;
		}*/
									  
									  
		for (int tmp = 0; tmp < rozmiar; tmp++)
		{
			if (tmp < rozmiar * 0.5)
			{
				tab[tmp] = tmp;
			}
			else {
				tab[tmp] = rand();
			}

		}
		/*cout << "Wypisanie tablicy:" << endl;
		for (int tmp = 0; tmp < rozmiar; tmp++)
		{
			cout << tab[tmp] << " ";
		}*/
		QueryPerformanceFrequency(&frequency);

		// start timer
		QueryPerformanceCounter(&t1);
		Shell(tab,rozmiar);
		// stop timer
		QueryPerformanceCounter(&t2);

		// compute and print the elapsed time in millisec
		elapsedTime = (t2.QuadPart - t1.QuadPart) * 1000.0 / frequency.QuadPart;
		cout << elapsedTime << " ms.\n";

	/*	cout << endl << "Wypisanie posortowanej tablicy:" << endl;
		for (int tmp = 0; tmp < rozmiar; tmp++)
		{
			cout << tab[tmp] << " ";
		}*/
		cout << endl;
		delete[] tab; //zwolnienie miejsca zajmowanego przez tablice
		system("PAUSE");
		return 0;
	}

